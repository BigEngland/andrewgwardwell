define([
  'backbone',
  'mustache',
  'utility',
  'app/views/MovieView',
  'app/views/MovieDetailView',
  'app/views/MovieCollectionView',
], function (Backbone, mustache, u, MovieView, MovieDetailView, MovieCollectionView) {
  var MovieCollectionViewCS = MovieCollectionView.extend({

    initialize: function () {
      this.listenTo(this.collection, 'reset', this.render);
//      this.listenTo(this.collection, 'sync', this.render);
      this.listenTo(this.collection, 'reset', this.movieGrid);
//      this.listenToOnce(this.collection, 'reset', this.initialFilter);
    },

    preprocess: function(data){
      this.collection.models = this.collection.sortBy(function(item){
        var releases = item.get('releases');
        var release = _.min(releases, function(item){
          return item;
        });
        return release;
      });
      this.collection.models = _.uniq(this.collection.models, function(item){
        var wr = item.get('writers');
        var ac = item.get('actors');
        var dr = item.get('directors');
        var title = item.get('title');
        var related_movies = item.get('related_movies');
        var hash = item.get('hash');
        var array = _.isArray(related_movies);
        if (related_movies != null && array){
          if(related_movies.length > 0){
            return hash;
          } else {
            return item.get('movie_id');
          }
        } else {
          return item.get('movie_id');
        }
      });
    },

    initialFilter: function(){
//      this.collection.filter(function(item) {
//        var releases = item.get('releases'),
//        // We need to reduce the collection to a version that has release dates after today
//        // this is utc and a unix timestamp to match the server.
//          today = moment().utc().format('X');
//        // Loop through the collection find those with release dates after today.
//        // Iterate through if any of them have a timestamp of greater than today return that one.
//        var to_come_release = _.some(releases, function(release){
//          return release > today;
//        });
//        return to_come_release;
//      });
    }
  });

  return MovieCollectionViewCS;
});
