define([
  'backbone',
  'mustache',
  'userLocation',
  'text!app/templates/theatredetailpage.html'
], function (Backbone, mustache, user, template) {
  var TheatreDetailOverView = Backbone.View.extend({
    el:$('#theatre_page'),
    template: template,

    currentLocation : {},

    eventsview: {
      collection: {}
    },

    events: {
      'click #theatre_events_month_tabs li' : 'filterMonth'
    },

    initialize: function () {
      // Function helps us to get our bearings
      this.currentLocation = user.hasPref();
      this.listenTo(this.collection, 'reset', this.render);
    },

    preprocess: function(data){
      // Get some data to be rendered into out overview template
      // Is there a better way to be getting the first model? It seems like this
      // could be a the view of the model with a nested collection -- except
      // I have to be able to filter via theatre
      data.theatre = this.collection.models[0].toJSON();
    },

    postprocess : function(){

    },

    // Render everything
    render: function() {
      var data = {};
      this.preprocess(data);
      this.$el.html(mustache.render(this.template, data));
      // Take care of our subviews so that when we reset the data in the collection
      // they are taken care of based on this views listening to the collection.
      if(data.theatre.show_events){
        this.$('#theatre_events').append(this.eventsview.el);
      }
      this.postprocess();
      return this;
    },

    filter: function(id){
      this.collection.filter(function(item){
        var h_id = item.get('house_id');
        return h_id == id;
      });
    },

    filterMonth: function(e){
      var id = $(e.currentTarget).data('month');
      this.eventsview.filterMonth(id);
    }
  });

  return TheatreDetailOverView;
});