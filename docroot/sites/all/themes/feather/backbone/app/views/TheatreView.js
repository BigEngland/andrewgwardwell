/**
 * Created by Awardwell on 2/18/14.
 */

define([
  'app/views/BaseView',
  'text!app/templates/theatre.html'
], function (BaseView, template) {
  var TheatreView = BaseView.extend({
    className: 'nearby_theater',
    id: function(){
      var id = this.model.get('theatre_id');
      return id;
    },
    template: template

  });

  return TheatreView;
});