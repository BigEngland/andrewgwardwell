define([
  'backbone',
  'mustache',
  'matchMedia',
  'waypoints',
  'waypointssticky',
  'text!app/templates/navview.html'
], function (Backbone, mustache, matchMedia, waypoints, waypointssticky, template){
  var NavView = Backbone.View.extend({
    template: template,

    initialize: function () {
      this.listenTo(this.collection, 'reset', this.render);
    },

    events:{
      "click .nav__item" : "filterSet",
      "click .name" : "menuView",
      "click #icon" : "menuView",
      "click .close-menu" : "closeMenu"
    },

    preprocess: function(data){
      data.name = "Andrew Wardwell";
      data.skills = _.uniq(_.flatten(this.collection.pluck('skills')));
    },

    render: function() {
      this.$el.html('');
      var data = {};
      this.preprocess(data);
      this.$el.html(mustache.render(this.template, data));
      this.postprocess();
      return this;
    },

    postprocess: function(){
      var height = $(window).height();
      $('.skills').data('height', height);
      $('.page').waypoint('sticky', {
        direction: 'down right',
        stuckClass: 'fixed-header',
        offset: -250
      });
    },

    filterSet : function(e){
      var target = $(e.currentTarget);
      var arg = target.data('skill');
      $('.active-item').removeClass('active-item');
      target.addClass('active-item');
      this.trigger('filter', arg);
    },

    menuView : function(e){
      var height = $('.skills').data('height');
      $('.skills').css('height', height);
      $('.page').addClass('menu_open');
    },

    closeMenu : function(e){
      $('.skills').css('height', 0);
      $('.page').removeClass('menu_open');
    }
  });

  return NavView;
});