define([
  'backbone',
  'mustache',
  'utility',
  'app/views/MovieView',
  'app/views/MovieDetailView',
], function (Backbone, mustache, u, MovieView, MovieDetailView) {
  var MovieCollectionView = Backbone.View.extend({
    tagName: 'ul',
    id: 'movie_listing',

    initialize: function () {
      this.listenTo(this.collection, 'reset', this.render);
      this.listenTo(this.collection, 'sync', this.render);
      this.listenTo(this.collection, 'reset', this.movieGrid);
    },

    events:{
      'click .movie a': 'highlightMovie',
      'click .close_slide': 'closeSlide'
    },

    preprocess: function(data){
      this.collection.models = this.collection.sortBy(function(item){
        var releases = item.get('releases');
        var release = _.max(releases, function(item){
          return item;
        });
        return release;
      });
     this.collection.models = _.uniq(this.collection.models, function(item){
        return item.get('hash');
      });
      this.collection.models.reverse();
    },

    render: function() {
      this.$el.html('');
      var data = {};
      this.preprocess(data);
      this.collection.each(function(item) {
        var view = new MovieView({
          model: item
        });
        this.$el.append(view.render().el);
      }, this);

      return this;
    },

    closeSlide : function(e){
      if ($('.user_has_loc').length > 0) {
        $('.user_has_loc .changer').trigger('hidePop');
      }
      var view_alias = this.detail;
      $('.movie_detail').slideUp(300, function(){
        view_alias.remove();
        $('.detail-active').removeClass('detail-active');
      });
    },

    highlightMovie : function(e){
      e.preventDefault();

      var target = e.currentTarget;
      var target_id = $(target).data('movie-id');
      var parent = $(target).parent('.movie');
      var arrow_left = parent[0].offsetLeft;
      var rent_wid = parent[0].clientWidth * .4;
      var arrow_pos = rent_wid + arrow_left;

      if (this.detail.model){
        // if there currently is a model attached to the view remove the view
        if(this.detail.model.id === target_id && $('.detail-active').length > 0){
          var same_model = true;
        }
        // Remove the current model
        this.closeSlide();
      }

      var model = this.collection.get(target_id);
      var to_el = parent.nextAll('.movie_detail').first();
      this.detail = new MovieDetailView({model: model});
      if (!same_model){
        // Work needs to be done on the effect of these items.
        to_el.hide().append(this.detail.render().el).slideDown(300).addClass('detail-active').find('.upArrow').css('left', arrow_pos+'px');
      }
    },

    movieGrid : function(){
      u.movieGrid.init('#movie_listing', '.movie');
    }
  });

  return MovieCollectionView;
});