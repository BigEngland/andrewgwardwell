define([
  'popover',
  'backbone',
  'mustache',
  'userLocation',
  'MapsUtility',
  'app/views/TheatreView',
  'text!app/templates/theatres.html'
], function (popover, Backbone, mustache, user, maps, TheatreView, template) {
  var TheatreOverView = Backbone.View.extend({
    el:$('#subnav_locations'),
    template: template,

    currentLocation : {},

    subview: {
      collection: {}
    },

    initialize: function () {
      // Function helps us to get our bearings
      this.currentLocation = user.hasPref();
      this.listenTo(this.collection, 'reset', this.render);
    },

    preprocess: function(data){
      // Get some data to be rendered into out overview template
      data.city = this.currentLocation.city;
      data.state = this.currentLocation.state;
    },

    postprocess: function(){
      // Create the map for our sidebar.
      maps.createMap($('#map_canvas'), $('#map_container'));
      this.$('#showtime_home_location .changer').popover({override : true});
    },

    // Render everything
    render: function() {
      var data = {};
      this.preprocess(data);
      this.$el.html(mustache.render(this.template, data));
      // Take care of our subviews so that when we reset the data in the collection
      // they are taken care of based on this views listening to the collection.
      this.$('.theatres').append(this.subview.render().el);
      this.$('#map_container').append(this.mapview.render().el);
      this.postprocess();
      return this;
    }
  });

  return TheatreOverView;
});