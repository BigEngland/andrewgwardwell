define([
  'app/views/BaseView',
  'underscore',
  'utility',
  'userLocation',
  'moment',
  'text!app/templates/moviedetailpage.html',
  'jquery.selectBoxIt'
], function (BaseView, _, u, user, moment, template) {
  var MovieOverView = BaseView.extend({
    template: template,

    events: {
      'change .movie_multi_format' : 'getOtherVersion'
    },

    initialize: function () {
      this.listenTo(this.model, 'change', this.render);
    },

    preprocess: function(data){
      user.getPref();
      // Logic for the showtimes button at the bottom of the movie detail
      if(user.pref){
      var house_id = user.pref.id;
      // see if it is at the users location
      if(house_id){
        var at_curr_location = _.some(this.model.get('showtimes'), function(item){
          return item.house_id == house_id;
        });
        //
        var theatre_data = _.find(Drupal.settings.showTheatres, function(item){
          return house_id == item.house_id;
        });
        if(typeof theatre_data != "object"){
          data.nothing_set = true;
        } else {
          var city = theatre_data.city,
            state = theatre_data.state;
          if (at_curr_location){
            link = theatre_data.showtime_link + '#highlight/movie_id/' + this.model.id;
            var current = {
              city: city,
              state: state,
              link: link
            }
            data.current = current;
          } else {
            var na = {
              city: city,
              state: state
            };
            data.n_a = na;
          }
        }
      }
      } else {
        // no pref is set
        data.nothing_set = true;
      }
      var multi = _.uniq(this.model.get('related_movies'));
      if(multi.length > 0){
        data.multi = true;
        data.multi_format = multi;
        data.multi_format.push({
          'movie_id' : this.model.get('nid'),
          'title': this.model.get('title')
        });
      }
    },

    postprocess : function(){
      this.$('.changer').popover({override : true});
      this.$('.user_need_loc .locForm').on('submit', function(e){
        user.locFormSubShowtimes(e);
      });
      if(this.$('.movie_multi_format').length > 0){
        this.$('.movie_multi_format').selectBoxIt({autoWidth: false});
        this.$('.movie_multi_format').data('selectBox-selectBoxIt').selectOption(this.model.get('nid'));
      }
      $('#movie_detail_page').data({'movie-id': this.model.get('movie_id')});
      u.videoSizing();
    },

    getOtherVersion : function(e){
      var val = e.currentTarget.value;
//      this.model.clear();
      this.$('.movie_multi_format').val(val);
      this.model.set({"nid": val}, {silent: true});
      var self = this;
      this.model.fetch({success: function(mod, resp, o){
        self.model.set(resp);
        Drupal.settings.movieDetails = resp;
      }});
    }

  });

  return MovieOverView;
});
