define([
  'app/views/BaseView',
  'backbone',
  'mustache',
  'app/views/ShowtimeView',
  'utility',
], function (BaseView, Backbone, mustache, ShowtimeView, u) {
  var ShowtimeCollectionView = Backbone.View.extend({
    tagName: 'ul',
    id: 'showtime_list',

    events: {
      'click .showtime_col_1 h3' : 'dropInfo'
    },

    initialize: function () {
      this.listenTo(this.collection, 'reset', this.render);
//      this.listenTo(this.collection, 'sync', this.render);
//      this.listenToOnce(this.collection, 'reset', this.initialFilter);
    },

    initialFilter: function() {
      // Call this only once otherwise we create an endless loop
      var searches = {
        genre: 'All Genres',
        date: 'Today',
        rating: 'All Ratings'
      };
      this.collection.filter(function (item) {
        // Get the selected values for the filter (DOM elements)
        var movie = item.get('movie'),
          genres = movie.genre,
          rating = movie.rating,
          dates = item.get('showtimes'),
          hasGenre =  _.contains(genres, searches.genre),
          hasRating = rating == searches.rating;
        if (searches.date == 'Today'){
          searches.today = 'Today';
          searches.date = moment().format('ddd, MMM Do');
        }
        var hasDate = _.sortBy(_.filter(dates, function(date){
          var models_date = moment(date.showtime, 'X').utc().format('ddd, MMM Do');
          return (models_date == searches.date);
        }), function (item) {
          if (moment(item.showtime, 'X').utc().format('HH') == '00') {
            return moment(item.showtime, 'X').utc().add('days', 1).format('X');
          } else {
            return item.showtime;
          }
        });
        item.set('display_showtimes', hasDate);
        // If our all option is selected for one of the elements
        if (searches.genre == 'All Genres') {hasGenre = true;}
        if (searches.rating == 'All Ratings') {hasRating = true;}
        if (hasDate.length > 0) {hasDate = true;} else {hasDate = false;}
        return hasGenre && hasRating && hasDate;
      });
      if (searches.today) {
        searches.date = searches.today;
        delete searches.today;
      }
    },

    render: function() {
      this.$el.html('');
      this.collection.each(function(item) {
        var view = new ShowtimeView({
          model: item
        });
        this.$el.append(view.render().el);
      }, this);

      return this;
    },

    dropInfo : function(e){
      var el = e.currentTarget,
          movie = $(el).data('movie-id'),
          parent = el.parentElement.parentElement.parentElement;
      if($(parent).hasClass('active') && $(parent).hasClass('is_open')){
        $(parent).find('#'+movie).slideUp(300, function(){
          $(parent).removeClass('active');
        });
      } else {
        $(parent).addClass('active').addClass('is_open').find('#'+movie).slideDown(300);
      }
    }

  });

  return ShowtimeCollectionView;
});