define([
  'app/views/MovieOverView',
  'underscore',
  'utility',
  'moment',
  'text!app/templates/movieoverviewcs.html'
], function (MovieOverView, _, u, moment, template) {
  var MovieOverViewCS = MovieOverView.extend({
    // Minor Change in template we are over riding here (minus one select box)
    template: template,

    initialize: function () {
      this.listenTo(this.collection, 'reset', this.render);
//      this.listenTo(this.collection, 'reset', this.initFilter);
      this.listenTo(this.collection, 'sync', this.render);
//      this.listenTo(this.collection, 'sync', u.movieGrid.init('#movie_listing', '.movie'));
    },

    preprocess: function(data) {
      // inherited functionality from the parent class
      data.ratings = _.filter(_.uniq(this.collection.pluck('rating')), function(item){
        return item;
      });
      data.genres = _.sortBy(_.map(_.filter(_.uniq(_.flatten(this.collection.pluck('genre'))), function(item){
        return _.contains(['Action-Adventure', 'Animation', 'Bollywood', 'Comedy', 'Documentary', 'Drama', 'Family', 'Program', 'Romance', 'SciFi-Fantasy', 'Suspense-Thriller'], item);
      }), function(item){
        return item.replace('-', '/').replace('Program', 'Event Cinema');
      }), function(item){
        return item;
      });
    },

    // Override of parent class post process ie removing a filter from the form (date)
    postprocess: function() {
      var filters = window.location.hash.replace('#filter/', '');
      // Split the path into args -- they will be our filters
      var allTheThings = u.pathToObj(filters);
      // Set values for the genre and the ratings
      this.$('#movie_filter_genre option[value="' + allTheThings.genre + '"]').prop('selected', true);
      this.$('#movie_filter_rating option[value="' + allTheThings.rating + '"]').prop('selected', true);
    },

    // We override the filter functionality of the parent class as well.
    filter: function (e) {
      // Get the selected values for the filter (DOM elements)
      var searches = {
        genre: $('#movie_filter_genre').val().replace('/', '-').replace('Event Cinema', 'Program'),
        rating: $('#movie_filter_rating').val()
      };
      // This is the same as the parent with the exception of no date processing/filtering
      // for comments see MovieOverView.filter.
      this.childView.collection.filter(function(item) {
        var genres = item.get('genre'),
        releases = item.get('releases'),
        rating = item.get('rating');
        // We need to reduce the collection to a version that has release dates after today
        // this is utc and a unix timestamp to match the server.
          today = moment().utc().format('X');
        // Loop through the collection find those with release dates after today.
        // Iterate through if any of them have a timestamp of greater than today return that one.
        var to_come_release = _.some(releases, function(release){
            return release > today;
        });
        var hasGenre =  _.contains(genres, searches.genre);
        var hasRating = rating == searches.rating;
        if (searches.genre == 'All Genres') {hasGenre = true;}
        if (searches.rating == 'All Ratings') {hasRating = true;}
        return to_come_release && hasGenre && hasRating;
      });
      this.trigger('filter', 'filter/' + u.objToPath(searches));
    }

  });

  return MovieOverViewCS;
});
