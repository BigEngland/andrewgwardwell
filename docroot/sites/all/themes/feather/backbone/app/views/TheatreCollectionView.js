define([
  'backbone',
  'mustache',
  'userLocation',
  'underscore',
  'MapsUtility',
  'app/views/TheatreView'
], function (Backbone, mustache, user, _, map, TheatreView) {
  var TheatreCollectionView = Backbone.View.extend({

    initialize: function () {
      // Get Location information
      this.currentLocation = user.hasPref();
    },

    events : {
    // Bind a map recentering to each of the five closest locations
    'click .nearby_theater' : 'recenterMap'
    },

    currentLocation: {},

    preprocess : function (data){
      // Unfortunately our JSON doesn't include distances from so we have to set
      // some distance attributes on the 5 closest theatre models
      this.setDistances();
      // Now we need to sort by those distances
      this.sortByDistance();
      // Then add an index attribute to those models -- this is a filtered collection however.
      this.setIndexes();
    },

    render: function() {
      var data = {};
      // Get the collection ready to render
      this.preprocess(data);
      // boilerplate
      this.collection.each(function(item) {
        var view = new TheatreView({
          model: item
        });
        this.$el.append(view.render().el);
      }, this);
      this.postprocess();
      return this;
    },

    postprocess : function(){
      // Get the first theater in the DOM and give it nearest class
      this.$('.nearby_theater').first().addClass('nearest');
    },

    sortByDistance : function(){
      // Sort by the farthest away.
      var sorted = this.collection.sortBy(function(item){
        var dis = item.get('distance');
        return dis;
      }, this);
      // Set the models to the new order.
      this.collection.models = sorted;
    },

    setDistances : function(){
      // Nearby theatres
      var distances = this.currentLocation.nearby;
      // Iterate over the collection and set values on the models that are the distance away
      this.collection.each(function(item){
        // retrieve the house ids (only useful item in the nearby array)
        var house_id = item.get('house_id');
        var theatre_info = _.find(distances, function(v){
          return v[2] == house_id;
        });
        // Set the distances on the models that need them
        // @todo when we refactor the user location stuff make this more robust
        if(theatre_info){
          item.set({distance: Math.round( theatre_info[0] * 10 ) / 10});
        }
      }, this);
    },

    setIndexes : function(){
      // We need some indexes for the benefit of the view
      var i = 1;
      this.collection.forEach(function(item){
        item.set('index', i);
        ++i;
      });
    },

    recenterMap : function(e){
      // git the id this will map to a model id in the collection
     var id = e.currentTarget.id;
      // pull from the collection
     var data = this.collection.get(id);
      // utility function to recenter the map on the selected theatres lat and
      // lng.
     map.reCenter(data.get('lat'), data.get('lon'));
      // Update the styling
     this.$('.nearby_theater').removeClass('nearest');
     this.$('#'+id+'').addClass('nearest');
     var house_id = data.get('house_id');
     this.trigger('recenter', house_id);
    }
  });

  return TheatreCollectionView;
});