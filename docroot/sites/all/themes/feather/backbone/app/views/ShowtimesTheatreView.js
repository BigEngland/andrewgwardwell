define([
  'app/views/BaseView',
  'text!app/templates/showtimetheatre.html'
], function (BaseView, template) {
  var ShowtimeView = BaseView.extend({
//    tagName: 'li',
    template: template,

    events: {
      'click #theatre_title' : 'toggleTheatreDetails'
    },

    initialize: function () {
      this.listenTo(this.collection, 'reset', this.render);
    },

    preprocess: function(data){
      data.theatre = this.collection.models[0].toJSON();
    },

    toggleTheatreDetails : function(e){
      if(_.contains(e.currentTarget.classList, 'is_open')){
        $('.theatre_info_fold').slideUp(300);
        $('#theatre_title').removeClass('is_open');
      } else {
        $('.theatre_info_fold').slideDown(300);
        $('#theatre_title').addClass('is_open');
      }
    },

    filter: function(id){
      this.collection.filter(function(item){
       return item.get('house_id') == id;
      });
    }

  });

  return ShowtimeView;
});
