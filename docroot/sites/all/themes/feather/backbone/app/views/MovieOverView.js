define([
  'app/views/BaseView',
  'underscore',
  'utility',
  'moment',
  'text!app/templates/movieoverview.html'
], function (BaseView, _, u, moment, template) {
  var MovieOverView = BaseView.extend({
    tagName: 'div',
    className: 'movie_overview',
    template: template,

    events: {
      'change #movie_filter_genre': 'filter',
      'change #movie_filter_day': 'filter',
      'change #movie_filter_rating': 'filter'
    },

    initialize: function () {
      this.listenTo(this.collection, 'reset', this.render);
      this.listenTo(this.collection, 'sync', this.render);
//      this.listenTo(this.collection, 'sync', u.movieGrid.init('#movie_listing', '.movie'));
    },

    preprocess: function(data) {
      data.ratings = _.filter(_.uniq(this.collection.pluck('rating')), function(item){
        return item;
      });
      data.genres = _.sortBy(_.map(_.filter(_.uniq(_.flatten(this.collection.pluck('genre'))), function(item){
        return _.contains(['Action-Adventure', 'Animation', 'Bollywood', 'Comedy', 'Documentary', 'Drama', 'Family', 'Program', 'Romance', 'SciFi-Fantasy', 'Suspense-Thriller'], item);
      }), function(item){
        return item.replace('-', '/').replace('Program', 'Event Cinema');
      }), function(item){
        return item;
      });
      // We supply dates (formated in utc so that they match server utc) and that
      // span 7 dayes into the future -- since we are comparing in a formatted non-
      // second or minute specific fashion we are able to compare today with the
      // curent utc time.
      var releases = [
        'Today',
        moment().utc().add('days', 1).format('ddd, MMM Do'),
        moment().utc().add('days', 2).format('ddd, MMM Do'),
        moment().utc().add('days', 3).format('ddd, MMM Do'),
        moment().utc().add('days', 4).format('ddd, MMM Do'),
        moment().utc().add('days', 5).format('ddd, MMM Do'),
        moment().utc().add('days', 6).format('ddd, MMM Do'),
      ];
      data.releases = releases;
    },

    postprocess: function() {
      var filters = window.location.hash.replace('#filter/', '');
      var allTheThings = u.pathToObj(filters);
      this.$('#movie_filter_genre option[value="' + allTheThings.genre + '"]').prop('selected', true);
      this.$('#movie_filter_rating option[value="' + allTheThings.rating + '"]').prop('selected', true);
      this.$('#movie_filter_day option[value="' + allTheThings.rating + '"]').prop('selected', true);
//      this.trigger('postprocess');
    },

    filter: function (e) {
      // Get the selected values for the filter (DOM elements)
      var searches = {
        genre: $('#movie_filter_genre').val().replace('/', '-').replace('Event Cinema', 'Program'),
        date: $('#movie_filter_day').val(),
        rating: $('#movie_filter_rating').val()
      };

      this.childView.collection.filter(function(item) {
        // Find the genre from within the collections
        var genres = item.get('genre'),
            rating = item.get('rating'),
            dates = item.get('showtimes');
        // The Genres and the Dates are in an array format
        var hasGenre =  _.contains(genres, searches.genre);
        if (searches.date == 'Today'){
          searches.today = 'Today';
          searches.date = moment().format('ddd, MMM Do');
        }
        var hasDate =  _.filter(dates, function(date){
            var models_date = moment(date.showtime, 'X').utc().format('ddd, MMM Do');
            return (models_date == searches.date);
        });
        // The rating is a single value
        var hasRating = rating == searches.rating;
        // If our all option is selected for one of the elements
        if (searches.genre == 'All Genres') {hasGenre = true;}
        if (searches.rating == 'All Ratings') {hasRating = true;}
        if (hasDate.length > 0) {hasDate = true;} else {hasDate = false;}
        return hasGenre && hasRating && hasDate;
      });
      // Convert todays searches.date back into 'Today' if it has been set (for use in the URL)
      if(searches.today){
        searches.date = searches.today;
        delete searches.today;
      }
      // Trigger the filter even and pass the object to the
      this.trigger('filter', 'filter/' + u.objToPath(searches));
    }
  });

  return MovieOverView;
});